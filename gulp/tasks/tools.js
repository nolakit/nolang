var gulp = require('gulp'),
    express = require('express'),
    bodyParser = require('body-parser'),
    fs = require('fs'),
    junk = require('junk'),
    dir2read = './assets/i18n/',
    serverport = 5000;

//We only configure the server here and start it only when running the watch task

var server = express();
//Add livereload middleware before static-middleware
server.use( express.static('./tools/i18n') );
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json())

server.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "DELETE");
    next();
});

server.get( '/api/i18n', function( req, res ){
    var templates = [];

    fs.readdir( dir2read, function( error, files ) {
        if ( error ) {
            console.log("Error listing file contents.");
            res.status(404).json( { error : "error listing file contents for repository." } );
        }
        else
        {
            var totalBytes = 0;

            // This function repeatedly calls itself until the files are all read.
            var readFiles = function(index) {
                if ( index == files.filter(junk.not).length ) {
                    // we are done.
                    console.log( "Done reading files. totalBytes = " + totalBytes );
                    res.send( templates );
                }
                else{
                    fs.readFile( dir2read+'/'+files.filter(junk.not)[index], 'utf-8', function( error, data ) {
                        if ( error ) {
                            console.log( "Error reading file. ", error );
                        } else {
                            tpl = {
                                name : files.filter(junk.not)[index].substr(0, files.filter(junk.not)[index].lastIndexOf('.')),
                                content : formatDecode( JSON.parse( data ) )
                            };
                            templates.push( tpl );
                            totalBytes += data.length;
                            readFiles(index + 1);
                        }
                    });
                }

            };
            readFiles(0);
        }
    });
});

server.post( '/api/i18n', function( req, res ){
    var self = this,
        obj = formatEncode(req.body.lang) || {};

    //save file
    saveFile( req.body.value, obj, res );
});

server.del( '/api/i18n', function( req, res ){
    fs.unlink( dir2read+req.body.lang+".json", function(err){
        if (err) res.send({message: "Erreur lors de la suppression", status: "ko"});
        else res.send({message: "Niquel!", status: "ok"});
    });
});

server.post( '/api/i18n/new', function( req, res ){
    fs.stat(dir2read+req.body.lang+".json", function(err, stat) {
        if(err == null)
        {
            res.send({message: "Le fichier existe !", status: "ko"});
        } else if(err.code == 'ENOENT')
        {
            fs.writeFile( dir2read+req.body.lang+".json", JSON.stringify({}, null, 4) );
            res.send({message: "Fichier créé", status: "ok"});
        } else {
            console.log('Some other error: ', err.code);
        }
    });
});

server.post( '/api/i18n/duplicate', function( req, res ){
    fs.readFile( dir2read+req.body.from+".json", 'utf-8', function( error, data ) {
        if ( error )
        {
            console.log( "Error reading file. ", error );
        }
        else
        {
            var skeleton = JSON.parse( data ),
                tpl = {};

            for (var key in skeleton)
            {
                tpl[key] = "";
            }

            saveFile( req.body.to, tpl, res );
        }
    });
});

server.post( '/api/i18n/download/:lang', function( req, res ){
    var csv = generateCSV( req.param('lang'), function( data )
    {
        var filename = req.param('lang')+'.csv';
        res.attachment(filename);
        res.end(data, 'UTF-8');
    });
});

gulp.task('tools', function() {
    //Set up your static fileserver, which serves files in the build dir
    server.listen(serverport);
});

function generateCSV( name, cb )
{
    var csv = 'Key;value\n';
    fs.readFile( dir2read+name+".json", 'utf-8', function( error, data ) {
        var formated = JSON.parse( data );
        for (var key in formated)
        {
            csv += ''+key+'\t;'+formated[key]+'\n';
        }
        cb( csv );
    });
}

function formatDecode( data )
{
    var decoded = [];
    for ( var key in data )
    {
        decoded.push({
            key : key,
            value : data[key]
        });
    }
    return decoded;
}


function formatEncode( data )
{
    var encoded = {},
        dat = JSON.parse(data);
    for (var i = 0; i < dat.length; i++) {
        encoded[dat[i].key] = dat[i].value;
    };
    return encoded;
}

function saveFile( name, content, res )
{
    fs.writeFile(dir2read+name+".json", JSON.stringify( content, null, 4 ), function(err)
    {
        if(err) {
            console.log(err);
        } else {
            res.header('Access-Control-Allow-Origin', '*');
            res.send({message : "ok"});
        }
    });
}