var gulp = require('gulp'),
browserSync = require('browser-sync'),
reload = browserSync.reload;

gulp.task('default', ['clean', 'jshint', 'styles', 'watch', 'tools']);
// gulp.task('dist', ...

// Watch Files For Changes & Reload
gulp.task('serve', ['clean', 'jshint', 'styles', 'watch', 'tools'], function () {
    browserSync.init(null, {
        watchTask: true,
        proxy: "nolang.dev:8888", //REMPLACE WITH YOUR VHOST
        notify: false
    });


    gulp.watch(['./build/**/*.*'], reload);
});