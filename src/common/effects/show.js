/*
    define specific transition
 */

var TweenMax = require('TweenMax');

module.exports = {
    enter: function (el, insert, timeout) {
        var index = el.getAttribute("data-index");
        insert();
        this.tlTransition2 = new TimelineMax();
        this.tlTransition2.fromTo(el, 0.7, {alpha: 0, y: 30}, {alpha: 1, y: 0, ease:Quint.easeOut}, 0.05*index);
    },
    leave: function (el, remove, timeout) {
        remove();
    }
};