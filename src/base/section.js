"use strict";
/*
    Section base object
 */
var TweenMax = require('TweenMax');

module.exports = {
    methods: {
        listen : function()
        {
            //tile is in viewport animate show
            this.$on('viewportenter', function ()
            {
                this.transitionIn();
            });
        },
        transitionIn: function()
        {
            this.$el.style.display = 'block';
            this.tlTransition.play();
        },
        createTimeline: function() {
            this.tlTransition = new TimelineMax({
                onComplete: this.onTransitionInComplete,
                onCompleteScope: this,
                paused: true
            });
        },
        onTransitionInComplete : function()
        {
        },
        insertTweens : function()
        {
            this.tlTransition.fromTo(this.$el, 1.5, {alpha: 0, y: 80}, {alpha: 1, y: 0, ease: Expo.easeOut});
        },
        domReady : function()
        {
            this.createTimeline();
            this.insertTweens(); // Override this
            this.listen();
        }
    },
    created : function()
    {
        this.$on('hook:ready', this.domReady);
    }
};