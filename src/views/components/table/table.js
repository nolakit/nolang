"use strict";
/*
    table component
 */

module.exports = {
    template: require('./table.html'),
    data : {
        values : null
    },
    methods : {
        display : function( values )
        {
            this.$data.values = values;
        },
        format : function( e )
        {
            e.target.value = e.target.value.replace( ' ', '_' );
        },
        save : function( index )
        {
            this.$dispatch( "lang:update", index );
        },
        add : function()
        {
            this.$data.values.push({
                key : "",
                value: ""
            });
        },
        remove : function( index )
        {
            this.$data.values.splice( index, 1 );
            this.$dispatch( "lang:update" );
        },
        duplicate : function()
        {
            if( this.$$.duplicate_inpute.value.length > 0 )
                this.$dispatch( 'lang:duplicate', this.$$.duplicate_inpute.value );
        }
    },
    ready: function()
    {
        this.$on( 'table:display', this.display );
    }
};