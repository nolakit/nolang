"use strict";
/*
    sidebar layout
 */

module.exports = {
    template: require('./sidebar.html'),
    methods : {
        fetch : function( name, index )
        {
            this.$data.item = name;
            this.$dispatch( 'lang:get', index );
        },
        add : function()
        {
            if( this.hasClass("show", ".add") )
            {
                this.removeClass("show", ".add");
            }
            else
            {
                this.addClass("show", ".add");
            }
        },
        saveAdd : function()
        {
            if( this.$$.input.value.length > 0 )
                this.$dispatch( 'lang:add', this.$$.input.value );
        },
        removeLang : function( name )
        {
            this.$dispatch( "lang:delete", name );
        }
    },
    filters : {
        isEgual : function( value )
        {
            return ( value == this.$parent.$data.item ) ? "active" : "";
        }
    },
    ready: function()
    {
    }
};