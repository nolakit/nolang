'use strict';

/*
    App entry point.

    Creates the top-most viewmodel,
    registers all components,
    and start on page load.
 */

var Vue = require('vue'),
    debug = require('vue-debug'),
    query = require('vue-query'),
    el = require('vue-el'),
    request = require('superagent');

Vue.use(debug);
Vue.use(query);
Vue.use(el);

function start() {
    fetch( function( response ){
        init( response );
    });
}

function fetch( cb )
{
    request
    .get( window.location.protocol+"//"+window.location.hostname+":5000/api/i18n" )
    .end(function( response )
    {
        cb( JSON.parse( response.text ) );
    });
}

function init( langs ) {
    var app = new Vue({
        el: 'body',

        data: {
            content : {
                langs : langs,
                current : null,
                item : null,
                download : window.location.protocol+"//"+window.location.hostname+":5000/api/i18n/download/"
            }
        },

        directives: {
            'detect-viewport': require('./common/directives/viewport')
        },

        components: {
            /* LAYOUT */
            "sidebar" : require('./views/layout/sidebar/sidebar'),

            /* COMPONENTs */
            "table" : require('./views/components/table/table')

            /* PAGES */

            /* COMMON */
        },

        effects: {
            "effect" : require('./common/effects/show')
        },

        ready: function()
        {
            this.$on('lang:get', function( index )
            {
                this.$data.content.current = index;
                this.$broadcast( 'table:display', this.$data.content.langs[index].content );
            });

            this.$on('lang:update', function()
            {
                request
                .post( window.location.protocol+"//"+window.location.hostname+":5000/api/i18n" )
                .send({
                    value: this.$data.content.langs[this.$data.content.current].name,
                    lang: JSON.stringify( this.$data.content.langs[this.$data.content.current].content )
                })
                .end(function( response )
                {
                    console.log( "update success" );
                });
            });

            this.$on('lang:add', function( value )
            {
                var self = this;

                request
                .post( window.location.protocol+"//"+window.location.hostname+":5000/api/i18n/new" )
                .send({
                    lang: value
                })
                .end(function( response )
                {
                    fetch( function( response ){
                        self.$data.content.langs = response;
                    });
                });
            });

            this.$on('lang:duplicate', function( value )
            {
                var self = this;
                request
                .post( window.location.protocol+"//"+window.location.hostname+":5000/api/i18n/duplicate" )
                .send({
                    from: this.$data.content.langs[this.$data.content.current].name,
                    to: value
                })
                .end(function( response )
                {
                    fetch( function( response ){
                        self.$data.content.langs = response;
                    });
                });
            });

            this.$on('lang:delete', function( code )
            {
                var self = this;
                request
                .del( window.location.protocol+"//"+window.location.hostname+":5000/api/i18n" )
                .send({
                    lang: code
                })
                .end(function( response )
                {
                    fetch( function( response ){
                        self.$data.content.langs = response;
                    });
                });
            });
        }
    });
}

window.onload = start;
